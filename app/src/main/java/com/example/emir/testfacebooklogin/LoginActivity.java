package com.example.emir.testfacebooklogin;

import android.content.Intent;
import android.media.MediaPlayer;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.emir.testfacebooklogin.User.User;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.jakewharton.threetenabp.AndroidThreeTen;

import org.threeten.bp.LocalDateTime;

import java.util.Arrays;


public class LoginActivity extends AppCompatActivity {

    private TextView info;
    private LoginButton loginButton;
    private ImageView loginImage;
    private LinearLayout loginHolder;
    private ProgressBar progressBarLogin;
    private ImageView exitLogin;

    private CallbackManager callbackManager;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    private User newUser;
    private MediaPlayer mp;
    private Animation animation;

    private LocalDateTime dateTimeNow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        AndroidThreeTen.init(this);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.activity_login);
        initialize();

        loginButton.setReadPermissions(Arrays.asList("public_profile", "user_relationships", "user_hometown", "email", "user_location", "user_birthday"));

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {

            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                info.setText("Login attempt canceled.");
                progressBarLogin.setVisibility(View.GONE);
                loginButton.setClickable(true);
            }

            @Override
            public void onError(FacebookException error) {
                info.setText("Login attempt failed.");
                progressBarLogin.setVisibility(View.GONE);
                loginButton.setClickable(true);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.start();
                progressBarLogin.setVisibility(View.VISIBLE);
                loginButton.setClickable(false);
            }
        });

        exitLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.this.finish();
            }
        });
    }

    private void handleFacebookAccessToken(AccessToken token) {

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            info.setText("");
                            FirebaseUser user = mAuth.getCurrentUser();
                            Toast.makeText(getApplicationContext(),"Wellcome "+user.getDisplayName(),
                                    Toast.LENGTH_LONG).show();
                            saveUser(user);
                            progressBarLogin.setVisibility(View.GONE);
                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            startActivity(intent);
                            finish();
                        }
                        else {
                            progressBarLogin.setVisibility(View.GONE);
                            info.setText("");
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            saveUser(null);
                        }
                    }
                });
    }

    private void saveUser(FirebaseUser user) {

        if (user != null) {
            dateTimeNow = LocalDateTime.now();
            newUser = new User(user.getDisplayName(),user.getEmail(),user.getPhoneNumber(),user.getPhotoUrl().toString(),dateTimeNow.toString());
            mDatabase.child("users").child(user.getUid()).setValue(newUser);
        }
        else {
            info.setText("User signed out...");
        }
    }

    private void initialize() {
        info = findViewById(R.id.info);
        loginButton = findViewById(R.id.login_button);
        loginImage = findViewById(R.id.login_img);
        loginHolder = findViewById(R.id.login_holder);
        progressBarLogin = findViewById(R.id.progressBarLogin);
        exitLogin = findViewById(R.id.exit_login);
        info.setText("");
        progressBarLogin.setVisibility(View.GONE);
        // Initialize Firebase Auth and Database
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mp = MediaPlayer.create(getApplicationContext(),R.raw.button_click);
        animation = AnimationUtils.loadAnimation(LoginActivity.this,R.anim.animate);
        loginHolder.startAnimation(animation);
        loginButton.setClickable(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        saveUser(currentUser);
        info.setText("");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
