package com.example.emir.testfacebooklogin.Item;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.example.emir.testfacebooklogin.R;
import com.squareup.picasso.Picasso;

import org.threeten.bp.LocalDateTime;
import org.threeten.bp.format.DateTimeFormatter;

public class CakeRecyclerAdapter extends RecyclerView.Adapter<CakeRecyclerAdapter.ViewHolder> {

    List<Cake> cakeList = new ArrayList<>();
    Context context;

    public CakeRecyclerAdapter(List<Cake> cakeList, Context context) {
        this.cakeList = cakeList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cake_list_item, parent, false);
        return new CakeRecyclerAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Cake cake = cakeList.get(position);

        holder.cakeTitle.setText(cake.getTitle());
        holder.cakeRecipe.setText(cake.getRecipe());
        holder.cakePrice.setText("Price: "+String.valueOf(cake.getPrice()));
        holder.cakeDateTime.setText(getFormattedDateTime(cake.getAddedAt()));

        Picasso.with(context)
                .load(cake.getImgUrl())
                .placeholder(R.drawable.cake_placeholder)
                .into(holder.cakeImage);
    }

    private String getFormattedDateTime(String addedAt) {

        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy HH:mm:ss");
        LocalDateTime ldt = LocalDateTime.parse(addedAt);

        return dtf.format(ldt);
    }

    @Override
    public int getItemCount() {
        return cakeList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView cakeTitle;
        private TextView cakeRecipe;
        private TextView cakePrice;
        private ImageView cakeImage;
        private TextView cakeDateTime;

        public ViewHolder(View itemView) {
            super(itemView);

            cakeTitle = itemView.findViewById(R.id.cake_list_item_title);
            cakeRecipe = itemView.findViewById(R.id.cake_list_item_recipe);
            cakePrice = itemView.findViewById(R.id.cake_list_item_price);
            cakeImage = itemView.findViewById(R.id.cake_list_item_img);
            cakeDateTime = itemView.findViewById(R.id.cake_date_time);

            cakeRecipe.setMovementMethod(new ScrollingMovementMethod());

        }
    }
}
