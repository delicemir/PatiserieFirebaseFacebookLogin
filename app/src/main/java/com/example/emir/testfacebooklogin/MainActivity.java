package com.example.emir.testfacebooklogin;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.emir.testfacebooklogin.Fragments.AddItemFragment;
import com.example.emir.testfacebooklogin.Fragments.DeleteItemFragment;
import com.example.emir.testfacebooklogin.Fragments.PreviewItemsFragment;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.facebook.login.widget.LoginButton;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private LoginButton logOffButton;
    private ImageView imgFb;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ScrollableTabsAdapter tabsAdapter;

    private FirebaseAuth auth;
    private FirebaseUser firebaseUser;

    private List<Fragment> fragmentList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialize();

        logOffButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(AccessToken.getCurrentAccessToken() != null) {
                    LoginManager.getInstance().logOut();
                    Intent intent = new Intent(getApplicationContext(),LoginActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        });

        setupTabPager();
    }

    private void setupTabPager() {

        tabsAdapter = new ScrollableTabsAdapter(getSupportFragmentManager(),fragmentList);
        viewPager.setAdapter(tabsAdapter);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.add).setText("Add new");
        tabLayout.getTabAt(1).setIcon(R.drawable.preview).setText("Preview");
        tabLayout.getTabAt(2).setIcon(R.drawable.delete).setText("Delete");

    }

    private void initialize() {
        logOffButton = findViewById(R.id.logoff_button);
        imgFb = findViewById(R.id.img_fb);
        auth = FirebaseAuth.getInstance();
        firebaseUser = auth.getCurrentUser();

        Picasso.with(this)
                .load(firebaseUser.getPhotoUrl())
                .placeholder(R.drawable.person_placeholder)
                .into(imgFb);

        viewPager = findViewById(R.id.viewPager);
        tabLayout = findViewById(R.id.tabs);

        fragmentList.add(new AddItemFragment());
        fragmentList.add(new PreviewItemsFragment());
        fragmentList.add(new DeleteItemFragment());
    }

}
