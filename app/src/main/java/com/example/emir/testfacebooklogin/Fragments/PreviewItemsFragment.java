package com.example.emir.testfacebooklogin.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.emir.testfacebooklogin.Item.Cake;
import com.example.emir.testfacebooklogin.Item.CakeRecyclerAdapter;
import com.example.emir.testfacebooklogin.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class PreviewItemsFragment extends Fragment {

    private RecyclerView cakeRecyclerList;
    private CakeRecyclerAdapter cakeRecyclerAdapter;
    private List<Cake> cakeList = new ArrayList<>();

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    private Cake cake;

    public PreviewItemsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_preview_items, container, false);

        cakeRecyclerList = view.findViewById(R.id.cake_recycler_list);

        //cake = new Cake();
        // Initialize Firebase Auth and Database
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        readAllCakes();

        mDatabase.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                readAllCakes();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getContext(),databaseError.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

        cakeRecyclerAdapter = new CakeRecyclerAdapter(cakeList,getContext());

        //cakeRecyclerList.setLayoutManager(new LinearLayoutManager(getContext()));
        cakeRecyclerList.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        SnapHelper snapHelper = new PagerSnapHelper();
        snapHelper.attachToRecyclerView(cakeRecyclerList);
        cakeRecyclerList.setAdapter(cakeRecyclerAdapter);
        cakeRecyclerAdapter.notifyDataSetChanged();

        return view;
    }

    private void readAllCakes() {

        mDatabase.child("recipes").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                cakeList.clear();

                for (DataSnapshot cakeSnapshot: dataSnapshot.getChildren()) {
                    cake = cakeSnapshot.getValue(Cake.class);
                    cakeList.add(cake);
                }
                cakeRecyclerAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(getContext(),databaseError.getMessage(),Toast.LENGTH_LONG).show();
            }
        });

    }


}
