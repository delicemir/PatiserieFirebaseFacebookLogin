package com.example.emir.testfacebooklogin.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.emir.testfacebooklogin.Item.Cake;
import com.example.emir.testfacebooklogin.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.threeten.bp.LocalDateTime;

public class AddItemFragment extends Fragment {

    private EditText cakeTitle;
    private EditText cakeRecipe;
    private EditText cakePrice;
    private EditText cakeImgUrl;
    private Button addNewCake;

    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;
    FirebaseUser user;

    private LocalDateTime dateTimeNow;
    private Cake newCake;

    public AddItemFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_item, container, false);

        cakeTitle = view.findViewById(R.id.cake_title);
        cakeRecipe = view.findViewById(R.id.cake_recipe);
        cakePrice = view.findViewById(R.id.cake_price);
        cakeImgUrl = view.findViewById(R.id.cake_image_url);
        addNewCake = view.findViewById(R.id.btn_add_new_cake);

        // Initialize Firebase Auth and Database
        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        user = mAuth.getCurrentUser();

        addNewCake.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(checkFormFields()) {
                    saveCake(user);
                    Toast.makeText(getContext(),"New cake added!",Toast.LENGTH_SHORT).show();
                    clearAllFormFields();
                }
                else {
                    Toast.makeText(getContext(),"You must fill all form fields!",Toast.LENGTH_SHORT).show();
                }
            }
        });

        return view;
    }

    private void clearAllFormFields() {

        cakeTitle.setText("");
        cakeRecipe.setText("");
        cakeImgUrl.setText("");
        cakePrice.setText("");

    }

    private boolean checkFormFields() {

        if(!cakeTitle.getText().toString().isEmpty() && !cakeRecipe.getText().toString().isEmpty()
                && !cakeImgUrl.getText().toString().isEmpty() && !cakePrice.getText().toString().isEmpty()) {
            return true;
        }

        return false;
    }

    private void saveCake(FirebaseUser user) {

        if (user != null) {
            dateTimeNow = LocalDateTime.now();
            newCake = new Cake(user.getUid(),cakeTitle.getText().toString(),cakeRecipe.getText().toString(),cakeImgUrl.getText().toString(),
                    Double.valueOf(cakePrice.getText().toString()),dateTimeNow.toString());
            mDatabase.child("recipes").child(newCake.getUniqueId()).setValue(newCake);
        }
        else {

        }
    }


}
