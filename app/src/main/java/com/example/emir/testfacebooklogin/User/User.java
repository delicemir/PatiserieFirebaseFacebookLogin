package com.example.emir.testfacebooklogin.User;

public class User {

    private String userId;
    private String displayName; // full name
    private String userEmail;
    private String userPhone;
    private String userPhotoUrl;
    private String loginAt;

    public User(String displayName, String userEmail, String userPhone, String userPhotoUrl, String loginAt) {
        this.displayName = displayName;
        this.userEmail = userEmail;
        this.userPhone = userPhone;
        this.userPhotoUrl = userPhotoUrl;
        this.loginAt = loginAt;
    }

    public User() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserPhone() {
        return userPhone;
    }

    public void setUserPhone(String userPhone) {
        this.userPhone = userPhone;
    }

    public String getUserPhotoUrl() {
        return userPhotoUrl;
    }

    public void setUserPhotoUrl(String userPhotoUrl) {
        this.userPhotoUrl = userPhotoUrl;
    }

    public String getLoginAt() {
        return loginAt;
    }

    public void setLoginAt(String loginAt) {
        this.loginAt = loginAt;
    }

    @Override
    public String toString() {
        return "User{" +
                "userId='" + userId + '\'' +
                ", displayName='" + displayName + '\'' +
                ", userEmail='" + userEmail + '\'' +
                ", userPhone='" + userPhone + '\'' +
                ", userPhotoUrl='" + userPhotoUrl + '\'' +
                ", loginAt='" + loginAt + '\'' +
                '}';
    }
}
