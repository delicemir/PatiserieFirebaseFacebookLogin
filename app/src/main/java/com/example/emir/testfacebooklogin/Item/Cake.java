package com.example.emir.testfacebooklogin.Item;

import java.util.UUID;

public class Cake {

    private String uniqueId;  //jedinstveni broj za svaki kolač
    private String userId; // id korisnika koji je dodao kolač - recept
    private String title;
    private String recipe;
    private String imgUrl;
    private Double price;
    private String addedAt;

    public Cake() {

    }

    public Cake(String userId, String title, String recipe, String imgUrl, Double price, String addedAt) {
        this.uniqueId = String.valueOf(UUID.randomUUID());
        this.userId = userId;
        this.title = title;
        this.recipe = recipe;
        this.imgUrl = imgUrl;
        this.price = price;
        this.addedAt = addedAt;
    }

    public Cake(String uniqueId, String userId, String title, String recipe, String imgUrl, Double price, String addedAt) {
        this.uniqueId = uniqueId;
        this.userId = userId;
        this.title = title;
        this.recipe = recipe;
        this.imgUrl = imgUrl;
        this.price = price;
        this.addedAt = addedAt;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public void setUniqueId(String uniqueId) {
        this.uniqueId = uniqueId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRecipe() {
        return recipe;
    }

    public void setRecipe(String recipe) {
        this.recipe = recipe;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAddedAt() {
        return addedAt;
    }

    public void setAddedAt(String addedAt) {
        this.addedAt = addedAt;
    }

    @Override
    public String toString() {
        return "Cake{" +
                "uniqueId=" + uniqueId +
                ", userId='" + userId + '\'' +
                ", title='" + title + '\'' +
                ", recipe='" + recipe + '\'' +
                ", imgUrl='" + imgUrl + '\'' +
                ", price=" + price +
                ", addedAt='" + addedAt + '\'' +
                '}';
    }
}
